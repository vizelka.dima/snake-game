import { WebApiLocationDatasource } from "./data/locationDataSource.js";
import { ApiWeatherDataSource } from './data/weatherDataSource.js';
import { LocalStorageSettingsDataSource } from './data/settingsDataSource.js';
import { LocalStorageScoresDataSource } from './data/scoresDataSource.js';
import ServiceContainer from "./data/ServiceContainer.js";
import Router from "./pages/routing/Router.js";
import { SettingsPage } from "./pages/SettingsPage.js";
import { MainMenuPage } from './pages/MainMenuPage.js';
import { ScoresPage } from './pages/ScoresPage.js';
import { GamePage } from './pages/GamePage.js';
import { NavigatorOnlineService } from "./data/onlineService.js";
import Game from './game/game.js';
import { CanvasGameDrawer } from "./game/ui/drawer.js";
import { KeyboardGameController } from "./game/extensions/controller.js";
import { WebAudioGameSound } from "./game/extensions/sound.js";
import { InMemoryGameMapDataSource } from "./data/gameMapDataSource.js";
import { basicMaps } from "./data/gameMaps.js";


(()=>{
    // SERVICE CREATORS
    const createWeatherService = () => {
        const API_KEY = 'd35de9f378114b25b82124544222405'
        const URL = `https://api.weatherapi.com/v1/current.json`
        
        return new ApiWeatherDataSource(
            {
                key: API_KEY,
                url: URL
            }
        )
    }
    
    const createLocationService = () => {
        return new WebApiLocationDatasource()
    }
    
    const createSettingsService = () => {
        return new LocalStorageSettingsDataSource('snake-game-settings')
    }
    
    const createScoresService = () => {
        return new LocalStorageScoresDataSource('snake-game-scores')
    }

    const createOnlineService = () => {
        return new NavigatorOnlineService()
    }

    const createGameMapService = () => {
        return new InMemoryGameMapDataSource(basicMaps())
    }

    const renderRoot = document.getElementById('root')
    const router = new Router(renderRoot)

    const locationService = createLocationService()
    const weatherService = createWeatherService()
    const onlineService =  createOnlineService()

    locationService
        .getCurrentCoords()
        .then((coords) => {
            weatherService.coords = coords
            weatherService.fetch()
        })
        .catch(()=>{})

    onlineService.addOnlineEvent(() => {
        weatherService.fetch()
    })

  

    // INJECT DEPENDENCIES (APP SERVICES)    
    const services = new ServiceContainer(
        router,
        createGameMapService(),
        weatherService,
        locationService,
        createSettingsService(),
        createScoresService(),
        onlineService
    )
    
    const defaultPageId = 'main'

    const snakeGameDrawer = new CanvasGameDrawer()
    const snakeGameController = new KeyboardGameController()
    const snakeGameSound = new WebAudioGameSound({
        eatSoundSrc: './sounds/sound_eat.wav'
    })
    const snakeGame = new Game(services, snakeGameDrawer, snakeGameController, snakeGameSound)

    const pages = [
        new MainMenuPage(defaultPageId, services),
        new ScoresPage('scores', services),
        new SettingsPage('settings', services),
        new GamePage('game', services, snakeGame)
    ]
    
    pages.forEach(p => router.register(p))
    
    router.setDefaultPage(defaultPageId)
    router.start()
})()

