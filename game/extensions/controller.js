import { Dir } from "../utils/directions.js"

class GameController {
    constructor() {
        this.currentSnakeDir = Dir.RIGHT
        this.dir = Dir.RIGHT
        this.cachedDir = Dir.RIGHT

        this.canChangeDir = true

        this.keyDirMap = {
            ArrowUp: Dir.UP,
            ArrowDown: Dir.DOWN,
            ArrowLeft: Dir.LEFT,
            ArrowRight: Dir.RIGHT
        }
    }
}

export class KeyboardGameController extends GameController {
    constructor() {
        super()
        
        this._registerKeyboardEvents()
    }

    _registerKeyboardEvents() {
        addEventListener('keydown', this._handleKeyDown.bind(this))
    }

    _handleKeyDown(e) {
        const dir = this.keyDirMap[e.key] || this.dir

        if(this.dir === Dir.NONE || ((dir + this.currentSnakeDir) % 2) ) {
            this.dir = dir
        } 
        else if(this.dir !== this.currentSnakeDir){
            this.cachedDir = dir
        }
    }

    reset(mapDir) {
        this.dir = mapDir || Dir.RIGHT
        this.cachedDir = mapDir || Dir.RIGHT
        this.currentSnakeDir = mapDir || Dir.RIGHT
    }
}