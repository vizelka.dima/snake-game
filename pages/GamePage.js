import ContextWindow from "../elements/ContextWindow.js";
import { createCardElem, createEmptyPageElem, createBasePageElem, createClipartElem, createLink } from "../elements/htmlContentFactory.js";
import Page from "./template/pageTemplates.js";

export class GamePage extends Page {
    constructor(id, services, game) {
        super(id, services)
        this._mapService = services.gameMapService
        this._elem = document.createElement('div')

        this._chooseMapElem = this._createChooseMapElem()
        this.chooseMap()

        this._scoreViewer = null
        this._contextWindow = null

        this._gameElem = this._createGameElem(game)
        this._game = game
    }

    _createChooseMapElem() {
        const {content, wrapper} = createBasePageElem('snake game')
    
        const innerContent = document.createElement('section')
        innerContent.classList.add('inner-content')
      
        const ul = document.createElement('ul')
        ul.classList.add('card-menu-list')

        this._mapService.list().forEach(({id, name})=>{
            const li = document.createElement('li')
            li.classList.add('clickable')
            li.textContent = `${name}`
            li.classList.add('card-map')

            li.addEventListener('click', (e) => {
                this._mapService
                    .find(id)
                    .then(m => {
                        this.startGame(m)
                    })
            })
            ul.appendChild(li)
        })
        

        const [card, back] = createCardElem('choose map', ul)
        innerContent.appendChild(card)
        
        back.addEventListener('click', (e) => {
            e.preventDefault()
            this._router.navigate('main')
        })

        const clipart = createClipartElem('./img/snake-clipart.png')

        wrapper.append(innerContent, clipart)

        return content
    }

    _createGameElem(game) {
        const {content, wrapper} = createEmptyPageElem()
        content.classList.add('game-container')
        game.renderTo(wrapper)

        const scoreViewer = document.createElement('div')
        scoreViewer.classList.add('score-viewer')
        scoreViewer.textContent = 0
        this._scoreViewer = scoreViewer

        wrapper.append(scoreViewer)
        game.registerScoreChangeEvent((newScore) => {
            scoreViewer.textContent = newScore
        })
        

        const gameOverContextWindow = new ContextWindow()
        this._contextWindow = gameOverContextWindow
        const gameOverContent = document.createElement('div')
        gameOverContent.classList.add('context-content')
        const gameOverTitle = document.createElement('h1')
        
        const toMenu = createLink(this._router, 'Menu', '/', ()=>{ gameOverContextWindow.hide()   })

        const toChooseMapMenu = document.createElement('a')
        toChooseMapMenu.textContent = 'Chooose Map'
        toChooseMapMenu.addEventListener('click', (e) => {
            e.preventDefault()
            gameOverContextWindow.hide()
            this.chooseMap()
        })

        const playAgain = document.createElement('a')
        playAgain.textContent = 'Play Again'
        playAgain.addEventListener('click', (e) => {
            e.preventDefault()
            game.restart()
            gameOverContextWindow.hide()
        })

        const contextMenu = document.createElement('ul')
        contextMenu.classList.add('context-menu')
        const liToMenu = document.createElement('li')
        liToMenu.appendChild(toMenu)

        const liToChooseMapMenu = document.createElement('li')
        liToChooseMapMenu.appendChild(toChooseMapMenu)

        const liPlayAgain = document.createElement('li')
        liPlayAgain.appendChild(playAgain)

        contextMenu.append(liToMenu, liToChooseMapMenu, liPlayAgain)
        gameOverContent.append(gameOverTitle, contextMenu)
        gameOverContextWindow.renderContent(gameOverContent)
        gameOverContextWindow.renderTo(content)

        game.registerGameOverEvent(() => {
            gameOverTitle.textContent = 'Game Over'
            gameOverContextWindow.show()
        })

        game.registerWinEvent(() => {
            gameOverTitle.textContent = 'You Won'
            gameOverContextWindow.show()
        })

        return content
    }

    chooseMap() {
        this._elem.innerHTML = ''
        this._elem.appendChild(this._chooseMapElem)
    }

    startGame(map) {
        this._elem.innerHTML = ''
        this._elem.appendChild(this._gameElem)
        this._game.start(map)
    }


    destroy() {
        if(this._contextWindow) {
            this._contextWindow.hide()
        }
        this._game.destroy()
        this.chooseMap()
    }

    get htmlContent () {
        return this._elem
    }
}