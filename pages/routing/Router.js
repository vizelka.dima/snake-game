
export default class Router {
    constructor(renderRoot) {
        this._routes = {}
        this._pageContainer = renderRoot

        this._currentRoute = null
        this._started = false

        this._defaultRouteId = null
    }

    _registerRoutes() {
        addEventListener('load', () => {this.navigate()})
        addEventListener('popstate', () => {this.navigate()});
    }

    setDefaultPage(pageId) {
        if(pageId in this._routes) {
            this._defaultRouteId = pageId
        }
    }

    register(page) {
        this._routes[page.id] = page
    }

    start() {
        if(this._started) {
            return
        }

        this._registerRoutes()
        this._started = true
    }

    back() {
        history.back()
    }


    navigate(page) {
        if(!this._started) { return }

        const queryString = window.location.search;
        const parameters = new URLSearchParams(queryString);
        let currentPage = parameters.get('page');
        
        if(page in this._routes) {
            currentPage = page
            history.pushState(null, null, `?page=${currentPage}`)
        }
        else if(page) {
            currentPage = this._defaultRouteId
            history.pushState(null, null, `?page=${currentPage}`)
        }
        else if(!(currentPage in this._routes) && (this._defaultRouteId in this._routes)){
            currentPage = this._defaultRouteId
            history.replaceState(null, null, `?page=${currentPage}`)
        }

        if(this._currentRoute) { this._currentRoute.destroy() }

        if(currentPage in this._routes){
            this._currentRoute = this._routes[currentPage]
            this._currentRoute.init()
            this._currentRoute.renderTo(this._pageContainer)
        }
    }
}
