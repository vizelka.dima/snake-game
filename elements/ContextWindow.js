export default class ContextWindow {
    constructor() {
        const overlay = document.createElement('div')
        overlay.classList.add('context-window')
        
        this._elem = overlay

        this.hide()
    }

    renderTo(container) {
        container.appendChild(this._elem)
    }

    remove() {
        this._elem.remove()
    }

    renderContent(contentElem) {
        this._elem.innerHTML = ''
        this._elem.append(contentElem)
    }

    show() {
        this._elem.classList.remove('hidden')
    }

    hide() {
        this._elem.classList.add('hidden')
    }
}


// CONTENT ELEM EXAMPLE
// this.title = document.createElement('h1')
// title.textContent = "GAME OVER"