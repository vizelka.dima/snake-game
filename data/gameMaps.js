
export const basicMaps = () => {
    return [
        {
            id: 0,
            name: 'No Walls Map - simple',
            content: {
                walls: {
                    vertical: false,
                    horizontal: false
                },
                foodCountTotal: 20,
                foodCountAtOnce: 1
            }
        },
        {
            id: 1,
            name: 'Walls Map - simple',
            content: {
                walls: {
                    vertical: true,
                    horizontal: true
                },
                foodCountTotal: 20,
                foodCountAtOnce: 1,
                initialFoodPos: [
                    {x: 13, y: 13}
                ]
            }
        },
        {
            id: 2,
            name: 'Walls Map [30]',
            content: {
                walls: {
                    vertical: true,
                    horizontal: true
                },
                foodCountTotal: 100,
                foodCountAtOnce: 30
            }
        },
        {
            id: 3,
            name: 'No Walls Map [30]',
            content: {
                walls: {
                    vertical: false,
                    horizontal: false
                },
                foodCountTotal: 100,
                foodCountAtOnce: 30
            }
        },
        {
            id: 4,
            name: 'Vertical Walls Map [30]',
            content: {
                walls: {
                    vertical: true,
                    horizontal: false
                },
                foodCountTotal: 100,
                foodCountAtOnce: 30
            }
        }
    ]
}