class LocationReadError extends Error {
    constructor(msg, cause) {
        super(msg)
        this.cause = cause
        this.name = 'Could not get location'
    }
}


class LocationDataSource {
    constructor() {
        this._coords = {lat: 0, long: 0}
    }

    async getCurrentCoords() {
        return Promise.resolve({...this._coords})
    }
}


export class WebApiLocationDatasource extends LocationDataSource {
    constructor(updateInterval) {
        super()

        this._allowedByUser = false
        this._lastUpdate = 0
        this._updateInterval = updateInterval || 60 * 60 * 1000 // == 1 hour
        this._loaded = false
    }

    async _promisifyGeolocation(options) {
        return new Promise((resolve, reject) => 
            navigator.geolocation.getCurrentPosition(resolve, reject, options)
        );
    }

    registerPosChangeEvents(successCb, failCb) {
        navigator.geolocation.watchPosition(
            (pos) => {
                this._coords.lat = pos.coords.latitude
                this._coords.long = pos.coords.longitude
                this._loaded = true
                successCb()
            },
            (error) => {
                if (error.code == error.PERMISSION_DENIED){
                    failCb()
                }
            });
    }

    async getCurrentCoords(isCached) {
        if(this._loaded && (isCached || ((Date.now() - this._lastUpdate) > 1000))) {
            return super.getCurrentCoords()
        }

        return this._promisifyGeolocation()
                    .then( pos => {
                        this._coords.lat = pos.coords.latitude
                        this._coords.long = pos.coords.longitude
                        this._loaded = true
                        return {...this._coords}
                    })
                    .catch(e => {
                        throw new LocationReadError(e.message)
                    })
    }
}
