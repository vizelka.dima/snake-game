# KAJ [semestrální práce] - Snake Game

- Link (gitlab pages): https://vizeldim.gitlab.io/snake-game

## Hra vytvorena dle navrhu
- https://www.figma.com/file/qXcGopBNH6qBDUrMF9sNrh/Untitled?node-id=0%3A1

## Cíl projektu
Cílem projektu bylo vytvoření klasické hry Snake.

## Popis
Aplikace se skládá z 4 stránek:
- Hlavní Menu (Main)
- Nastavení (Settings)
- Seznam nejlepších výsledků (Scores)
- Hra (Game)

## Popis funkčnosti
Stránky obsahují následující funkcionalitu:
- Hlavní Menu (Main)
    - Obsahuje svg animaci
- Nastavení (Settings) 
    - Vypnout/zapnout zvuk
    - Vypnout/zapnout barevné schéma dle aktuálního počasí v dané lokalitě 
- Hra (Game)
    - Po kliknutí na tlačítko 'Play' se zobrazí menu s volbou mapy (levelu). Po zvolení mapy se spustí hra. 
 
### Ovládání 
Hra(směr pohybu hada) se ovládá pomocí šipek na klávesnici.
